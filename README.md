# Библиотека для построения микросервисов с БД

## Пример использования

requirements.txt

```text
--extra-index-url https://gitlab.com/api/v4/groups/85480746/-/packages/pypi/simple common-fastapi=={версия}
```

## Линтинг кода и код-стайл

Установка для разработки

```sh
pip install -e ".[dev]"
```

Поднятие версии

```sh
bump-my-version bump {major/minor/patch} setup.py --tag --commit
```

Форматирование:

```sh
make format
```

Проверка:

```sh
make linting
```

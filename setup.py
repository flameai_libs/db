from setuptools import setup


setup(
    name="common_db",
    description="My pesonal code for creating DB components such as MySQL, Postgres, ClickHouse etc",
    version="0.1.1",
    author="Alexander Andryukov",
    author_email="andryukov@gmail.com",
    extras_require={
        "Postgres": [
            "asyncpg==0.28.0"
        ],
        "Redis": [
            "redis[hiredis]==5.0.0"
        ],

        # Auxiliary extras
        "dev": ["bump-my-version==0.20.0", "ruff==0.3.5"],
        "tests": ["pytest==8.1.1"]
    }
)

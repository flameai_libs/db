from __future__ import annotations

from contextlib import asynccontextmanager


class RedisMethodsFabric:  # TODO: Derive from common abstract ancestor
    def __init__(self, redis: str) -> None:
        self._redis = redis

    @asynccontextmanager
    async def while_key_was_not_changed(self, key: str) -> None:
        # Make observing context manager for particular key
        return
